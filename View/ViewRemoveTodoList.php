<?php

require_once __DIR__ . '/../Model/TodoList.php';
require_once __DIR__ . '/../Helper/Input.php';
require_once __DIR__ . '/../BusinessLogic/RemoveTodoList.php';

require_once __DIR__ . '/../View/ViewShowTodoList.php';

function viewRemoveTodoList() {
    echo "Menghapus Todo" .PHP_EOL;

    $todo = input('No. Todo (x untuk batal)');

    switch ($todo) {
        case 'x':
            viewShowTodoList();
            break;

        default:
            $success = removeTodoList($todo);

            if ($success) {
                echo "Sukses menghapus todo nomor $todo" . PHP_EOL;
                viewShowTodoList();
            } else {
                echo "Gagal menghapus todo nomor $todo" . PHP_EOL;
                viewShowTodoList();
            }
            break;
    }
}
