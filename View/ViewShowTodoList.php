<?php
require_once __DIR__ . '/../Model/TodoList.php';
require_once __DIR__ . '/../Helper/Input.php';
require_once __DIR__ . '/../BusinessLogic/ShowTodoList.php';

require_once __DIR__ . '/../View/ViewAddTodoList.php';
require_once __DIR__ . '/../View/ViewRemoveTodoList.php';

function viewShowTodoList() {
    while (true) {
        showTodoList();

        echo "MENU" . PHP_EOL;
        echo "1. Tambah Todo" . PHP_EOL;
        echo "2. Hapus Todo" . PHP_EOL;
        echo "x. Keluar" . PHP_EOL;

        $pilihan = input("Pilihan");

        switch ($pilihan) {
            case "1":
                viewAddTodoList();
                break;

            case "2":
                viewRemoveTodoList();
                break;

            case "x":
                echo "Sampai Jumpa Lagi!" . PHP_EOL;
                break;

            default:
                echo "Pilihan tidak diketahui" . PHP_EOL;
                break;
        }
        break;
    }
}
