<?php

namespace View {

    require_once __DIR__ . '/../../Entity/TodoList.php';
    require_once __DIR__ . '/../../Helper/Class/InputHelper.php';
    require_once __DIR__ . '/../../Repository/TodoListRepository.php';
    require_once __DIR__ . '/../../Service/TodoListService.php';

    use Helper\InputHelper;
    use Repository\TodoListRepositoryImpl;
    use Service\TodoListServiceImpl;

    class TodoListView
    {

        private TodoListServiceImpl $todoListService;

        public function __construct()
        {
            $todoListRepository = new TodoListRepositoryImpl();
            $this->todoListService = new TodoListServiceImpl($todoListRepository);
        }

        function showTodoList(): void {
            while (true) {
                $this->todoListService->showTodoList();

                echo "MENU" . PHP_EOL;
                echo "1. Tambah Todo" . PHP_EOL;
                echo "2. Hapus Todo" . PHP_EOL;
                echo "x. Keluar" . PHP_EOL;

                $pilihan = InputHelper::input("Pilihan");

                switch ($pilihan) {
                    case "1":
                        $this->addTodoList();
                        break;

                    case "2":
                        $this->removeTodoList();
                        break;

                    case "x":
                        echo "Sampai Jumpa Lagi!" . PHP_EOL;
                        break;

                    default:
                        echo "Pilihan tidak diketahui" . PHP_EOL;
                        $this->showTodoList();
                        break;
                }
                break;
            }
        }

        function addTodoList(): void {
            echo "Menambah Todo" . PHP_EOL;

            $todo = InputHelper::input('Todo (x untuk batal)');

            switch ($todo) {
                case 'x':
                    $this->showTodoList();
                    break;

                default:
                    $this->todoListService->addTodoList($todo);
                    $this->showTodoList();
                    break;
            }
        }

        function removeTodoList(): void {
            echo "Menghapus Todo" .PHP_EOL;

            $todo = InputHelper::input('No. Todo (x untuk batal)');

            switch ($todo) {
                case 'x':
                    $this->showTodoList();
                    break;

                default:
                    $this->todoListService->removeTodoList($todo);
                    $this->showTodoList();
                    break;
            }
        }
    }

}
