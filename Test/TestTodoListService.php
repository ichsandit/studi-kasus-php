<?php

require_once __DIR__ . '/../Config/Database.php';
require_once __DIR__ . '/../Entity/TodoList.php';
require_once __DIR__ . '/../Repository/TodoListRepository.php';
require_once __DIR__ . '/../Service/TodoListService.php';

use Repository\TodoListRepositoryImpl;
use Service\TodoListServiceImpl;

function testShowTodoList(): void {
    $todoListRepository = new TodoListRepositoryImpl();
    $todoListService = new TodoListServiceImpl($todoListRepository);

    $todoListService->showTodoList();
}

function testAddTodoList(): void {
    $todoListRepository = new TodoListRepositoryImpl();
    $todoListService = new TodoListServiceImpl($todoListRepository);

    $todoListService->addTodoList("Todo 1");

    $todoListService->showTodoList();
}

function testRemoveTodoList(): void {
    $todoListRepository = new TodoListRepositoryImpl();
    $todoListService = new TodoListServiceImpl($todoListRepository);

    $todoListService->addTodoList("Todo 1");

    $todoListService->showTodoList();

    $todoListService->removeTodoList(1);

    $todoListService->showTodoList();
}

testShowTodoList();
