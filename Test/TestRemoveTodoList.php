<?php

require_once __DIR__ . '/../Model/TodoList.php';
require_once __DIR__ . '/../BusinessLogic/AddTodoList.php';
require_once __DIR__ . '/../BusinessLogic/ShowTodoList.php';
require_once __DIR__ . '/../BusinessLogic/RemoveTodoList.php';

addTodoList('Eko');
addTodoList('Kurniawan');
addTodoList('Khannedy');

showTodoList();

removeTodoList(4);
showTodoList();
