<?php

require_once __DIR__ . '/../View/Class/TodoListView.php';

use View\TodoListView;

$todoListView = new TodoListView();
$todoListView->showTodoList();
