<?php

namespace Service {

    use Entity\TodoList;
    use Repository\TodoListRepository;

    interface TodoListService {

        function showTodoList(): void;

        function addTodoList(string $todo): void;

        function removeTodoList(int $number): void;

    }

    class TodoListServiceImpl implements TodoListService {

        private TodoListRepository $todoListRepository;

        public function __construct(TodoListRepository $todoListRepository)
        {
            $this->todoListRepository = $todoListRepository;
        }

        function showTodoList(): void
        {
            echo "TODOLIST" . PHP_EOL;

            $todoList = $this->todoListRepository->findAll();
            foreach ($todoList as $key => $value) {
                echo $value->getId() . ". ". $value->getTodo() . PHP_EOL;
            }
        }

        function addTodoList(string $todo): void
        {
            $todoList = new TodoList(null, $todo);
            $this->todoListRepository->save($todoList);
            echo "Sukses Menambah Todo List" . PHP_EOL;
        }

        function removeTodoList(int $number): void
        {
            $result = $this->todoListRepository->remove($number);
            if ($result) {
                echo "Sukses menghapus todo no $number" . PHP_EOL;
            } else {
                echo "Gagal menghapus todo no $number" . PHP_EOL;
            }
        }

    }

}
