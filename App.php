<?php

require_once __DIR__ . '/Config/Database.php';
require_once __DIR__ . '/View/Class/TodoListView.php';

use View\TodoListView;

$todoListView = new TodoListView();

echo "Aplikasi Todo List Sederhana" . PHP_EOL;

$todoListView->showTodoList();
