<?php

namespace Repository {

    use Entity\TodoList;
    use Config\Database;

    interface TodoListRepository {

        function save(TodoList $todoList): ?TodoList;

        function remove(int $number): bool;

        function findAll(): array;

    }

    class TodoListRepositoryImpl implements TodoListRepository {

        private array $todoList = array();

        private \PDO $database;

        public function __construct()
        {
            $this->database = Database::connection();
        }

        public function __destruct()
        {
            unset($this->database);
        }

        function save(TodoList $todoList): ?TodoList
        {
            $sql = "insert into todolist(todo) values(?)";
            $result = $this->database->prepare($sql);

            $success = $result->execute(
                [
                    $todoList->getTodo()
                ]
            );

            if ($success) {
                $id = $this->database->lastInsertId();
                $todoList->setId($id);

                return $todoList;
            } else {
                return null;
            }
        }

        function remove(int $id): bool
        {
            $sql = "delete from todolist where id = ?";
            $result = $this->database->prepare($sql);

            $success = $result->execute(
                [
                    $id
                ]
            );

            if ($success) {
                return true;
            } else {
                return false;
            }
        }

        function findAll(): array
        {
            $sql = "select * from todolist";
            $result = $this->database->query($sql);

            $array = [];

            foreach ($result->fetchAll() as $row) {
                $array[] = new TodoList(
                    $row['id'],
                    $row['todo']
                );
            }

            return $array;
        }
    }

}
