<?php

namespace Config {

    class Database {

        static string $host = 'localhost';
        static int $port = 3306;
        static string $username = 'root';
        static string $password = '';
        static string $dbname = 'belajar_php_todolist';

        static function connection(): \PDO {
            return new \PDO("mysql:host=" . self::$host . ":" . self::$port . ";dbname=" . self::$dbname, self::$username, self::$password);
        }

    }

}
